#!groovy

def branch = env.JENKINS_GITPUSH_BRANCH
def slackChannel = env.JENKINS_GITPUSH_SLACK_LIST

def ecrRegistryURL = '121749245160.dkr.ecr.us-west-2.amazonaws.com'
def dockerImage = '/ddo-jenkins-slave-centos-saas:latest'

def jenkinsCredentialId = '92bc15d4-cefb-4555-abf6-9b8b6e432fac'
def repositoryUrl = 'ssh://git@bitbucket.org:sistertravis/impact-profile.git'

def author;

node ('docker-host') {
  step([$class: 'WsCleanup'])
  timestamps {
    stage('Checkout') {
      git(
        branch: "${branch}",
        credentialsId: "${jenkinsCredentialId}",
        url: "${repositoryUrl}"
      )
    }
    docker.image(ecrRegistryURL + dockerImage).inside(){
      try {
        stage('Get Author') {
          def emailId = sh(
            script: "git log -1 --pretty=format:'%ae'",
            returnStdout: true
          );
          author='@'+emailId.take(emailId.indexOf('@'));
          println author
        }

        stage('Install') { sh 'npm install' }
        stage('Test') { sh 'npm run test:jenkins'  }

        stage('Package') { sh 'npm run package' }
        stage('Deploy Dev Environment'){
          withCredentials([[
              $class: 'UsernamePasswordMultiBinding',
              credentialsId: 'platform.deployer.creds',
              usernameVariable: 'ACCESS_KEY',
              passwordVariable: 'SECRET_KEY'
           ]]) {
             def awsEnv = "AWS_ACCESS_KEY_ID=${ACCESS_KEY} AWS_SECRET_ACCESS_KEY=${SECRET_KEY} AWS_DEFAULT_REGION=us-west-2"
             def deployCmd = "npm run deploy:impact-profile:local"
             sh "${awsEnv} ${deployCmd}"
           }
        }

        stage('IntegrationTest'){
          try {
            sh 'npm run test-e2e:jenkins'
          } catch (Exception err) {
            currentBuild.result = 'FAILURE'
            stage('Rollback deployment'){
              withCredentials([[
                  $class: 'UsernamePasswordMultiBinding',
                  credentialsId: 'platform.deployer.creds',
                  usernameVariable: 'ACCESS_KEY',
                  passwordVariable: 'SECRET_KEY'
               ]]) {
                 def awsEnv = "AWS_ACCESS_KEY_ID=${ACCESS_KEY} AWS_SECRET_ACCESS_KEY=${SECRET_KEY} AWS_DEFAULT_REGION=us-west-2"
                 def deployCmd = "npm run deploy:impact-profile:artifactory"
                 sh "${awsEnv} ${deployCmd}"
               }
            }
            throw err
          }
        }
        stage('Publish'){ sh 'npm run publish:patch' }

      } catch (Exception err) {
        currentBuild.result = 'FAILURE'
        throw err
      } finally {
        sendSlack([author , slackChannel])
      }
    }
  }
}

def sendSlack(channels) {
  if (currentBuild.result == null) {
    currentBuild.result = 'SUCCESS'
    if (isBackToNormal()) {
      sendToSlack('#0000FF', "BACK TO NORMAL", channels)
    } else {
      sendToSlack('#00FF00', "SUCCESS", channels)
    }
  } else if (isFailure()) {
    sendToSlack('#FF0000', "FAILURE", channels)
  }
}

def isBackToNormal() {
  return currentBuild?.previousBuild?.result != 'SUCCESS' && env.BUILD_NUMBER != 1
}

def isFailure() {
  return currentBuild.result == 'FAILURE'
}

def sendToSlack(color, status, channels) {
  def message = "Status: ${status} " +
    "(<${env.BUILD_URL}|Open>)\n" +
    "Service: `Platform Ui`\n" +
    "Branch: `${env.JENKINS_GITPUSH_BRANCH}`\n" +
    "Build number: `#${env.BUILD_NUMBER}`\n"
  channels.each {
    slackSend( color: color, channel: it, message: message)
  }
}
