// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

// Chrome needs to headless in Jenkins mode
// https://github.com/angular/protractor/blob/master/docs/browser-setup.md#using-headless-chrome

const {SpecReporter} = require('jasmine-spec-reporter');
const {join} = require('path');
const isJenkinsEnv = process.env.NODE_ENV === 'jenkins';
const log4js = require('log4js');

const capabilitiesJenkins = {
  browserName: 'chrome',
  acceptInsecureCerts: true,
  chromeOptions: {
    args: [
      "--headless",
      "--no-sandbox",
      "--disable-dev-shm-usage",
      "--disable-gpu",
      "--window-size=1280,1600",
      "--disable-web-security",
      "--disable-application-cache"]
  }
};


beforeLaunch: () => {
  log4js.configure({
    appenders: {
      fileLog: {type: "file", filename: "./Logs/ExecutionLog.log"},
      console: {type: "log4js-protractor-appender"}
    },
    categories: {
      file: {appenders: ['fileLog'], level: 'error'},
      default: {appenders: ['console', 'fileLog'], level: 'trace'}
    }
  })
};



const capabilitiesDefault = {
  browserName: 'chrome',
};

const capabilities = isJenkinsEnv ? capabilitiesJenkins : capabilitiesDefault;

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],
  capabilities: capabilities,
  directConnect: true,
  baseUrl: 'http://localhost:4200',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () {
    }
  },
  onPrepare() {
    browser.logger = log4js.getLogger('protractorLog4js');
    require('ts-node').register({
      project: join(__dirname, './tsconfig.e2e.json'),
    });
    require('tsconfig-paths/register');
    jasmine.getEnv().addReporter(new SpecReporter({spec: {displayStacktrace: true}}));
  }
};
