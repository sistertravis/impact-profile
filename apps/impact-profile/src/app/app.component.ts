import { Component } from '@angular/core';

@Component({
  selector: 'impact-profile-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'apps-impact-profile';
}
